module.exports = {
  title: 'magma',
  description: 'This is the magma guide',
  base: '/magma-guide/',
  dest: 'public',
  head: [
    ['link', { rel: 'icon', href: '/favicon.ico' }],
    // XXX https://code.luasoftware.com/tutorials/web-development/generate-favicon-for-website/
    ['meta', { name: 'theme-color', content: '#3eaf7c' }],
    ['meta', { name: 'apple-mobile-web-app-capable', content: 'yes' }],
    ['meta', { name: 'apple-mobile-web-app-status-bar-style', content: 'black' }],
    ['meta', { name: 'msapplication-TileColor', content: '#000000' }]
  ],
  themeConfig: {
    // XXX Fix to not display the title and enlarge the image
    //logo: '/magma-header.svg',
    repo: 'https://gitlab.com/lavafeld/magma-guide',
    repoLabel: 'Contribute!',
    editLinks: true,
    docsDir: 'docs',
    smoothScroll: true,
    editLinkText: 'Help us improve this page!',
    lastUpdated: 'Last Updated',
    sidebar: {
      sidebarDepth: 2,
      '/guide/': getGuideSidebar('Guide', 'Advanced'),
    },
    nav: [
      { text: 'Home', link: '/' },
      { text: 'Guide', link: '/guide/' },
      { text: 'Publications', link: '/publications/' }
    ]
  },
  plugins: {
      '@vuepress/back-to-top': true,
      '@vuepress/last-updated': true,
      '@vuepress/medium-zoom': true,
      'vuepress-plugin-export': true
  }
}

function getGuideSidebar (groupA, groupB) {
  return [
    {
      title: groupA,
      collapsable: false,
      children: [
        '',
        'getting-started',
        'testing-methodology',
        'network-measurements',
        'data-analysis',
        'run-measurements'
      ]
    },
    {
      title: groupB,
      collapsable: false,
      children: [
        // XXX Add an info page for the advanced section
        'analyze-dns',
        'analyze-google-traffic-data',
        'cable-visualization',
        'data-sources',
        'osint-sources'
      ]
    }
  ]
}
