---
home: true
heroImage: /about.jpg
tagline: Welcome to the magma guide!
actionText: Get Started →
actionLink: /guide/
features:
- title: 1st guide
  details: Research and documentation on network measurements, information controls and surveillance.
- title: Open collaborative structure
  details: Everyone can (re)use, distribute, modify, contribute and review the content of the guide.
- title: Merit
  details: Suitable for research fellows, journalists, human rights activists, lawyers, network engineers, technologists and any person working on these topics.
footer: Creative Commons Attribution Non Commercial 4.0 International
---
