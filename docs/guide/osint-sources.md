# OSINT sources

A list of sources to quickly and easily search IPs, domains, file hashes and
URLs using free Open Source Intelligence (OSINT) resources.

## BGP

* [BGPView](https://bgpview.io/) -- Debug and investigate information about IP
  addresses, ASN, IXs, BGP, ISPs, Prefixes and Domain names

<!-- TODO Check URLS -->

## Domains

* [Alexa Domain](https://www.alexa.com/siteinfo/)
* [BlueCoat Domain](http://sitereview.bluecoat.com/#/lookup-result/)
* [Censys Domain](https://censys.io/domain?q=)
* [FortiGuard Domain](http://fortiguard.com/search?q=)
* [MX Toolbox Domain](https://mxtoolbox.com/SuperTool.aspx?action=mx%3a)
* [Onyphe Domain](https://www.onyphe.io/search/?query=)
* [Pulsedive Domain](https://pulsedive.com/indicator/?ioc=)
* [SecurityTrails Domain](https://securitytrails.com/search/domain/) --
  Historical DNS data
* [Shodan Domain](https://www.shodan.io/search?query=)
* [Talos Domain](https://talosintelligence.com/reputation_center/lookup?search=)
* [ThreatCrowd Domain](https://www.threatcrowd.org/pivot.php?data=)
* [ThreatMiner Domain](https://www.threatminer.org/domain.php?q=)
* [TOR Domain](https://metrics.torproject.org/rs.html#search/)
* [VT Domain](https://virustotal.com/#/domain/)
* [X-Force Domain](https://exchange.xforce.ibmcloud.com/url/)

## DNS

* [DNSdumpster](https://dnsdumpster.com/) -- DNS recon & research, find & lookup
  dns records
* [DNS Propagation Checker](https://dnspropagationtool.com) -- Check DNS of a
  domain name from multiple DNS nameservers and resolvers around the world.
* [ViewDNS](https://viewdns.info) -- Gather a large amount of data about a given
  website or IP address
* [DNS Census 2013](https://archive.org/details/dns-census2013) -- A public
  dataset of registered domains and DNS records it contains about 2.5 billion
  DNS records gathered in the years 2012-2013.
* [Domains Index datasets](https://domains-index.com/downloads/category/free/)
  -- A list of zone files available for free download.
* [DNSApe](https://www.dnsape.com) -- A collection of (mainly) DNS and other
  network tools

## IPs

* [AbuseIPDB](https://www.abuseipdb.com/check/)
* [Alien IP](https://otx.alienvault.com/indicator/ip/)
* [Bad Packets IP](https://mirai.badpackets.net/?ipAddress=)
* [Censys IP](https://censys.io/ipv4/)
* [FortiGuard IP](http://fortiguard.com/search?q=)
* [GreyNoise IP](https://viz.greynoise.io/ip/)
* [IPVoid IP](http://www.ipvoid.com/)
* [Onyphe IP](https://www.onyphe.io/search/?query=)
* [Pulsedive IP](https://pulsedive.com/indicator/?ioc=)
* [SecurityTrails IP](https://securitytrails.com/search/domain/)
* [Shodan IP](https://www.shodan.io/host/)
* [Talos IP](https://talosintelligence.com/reputation_center/lookup?search=)
* [ThreatCrowd IP](https://www.threatcrowd.org/pivot.php?data=)
* [ThreatMiner IP](https://www.threatminer.org/host.php?q=)
* [Tor IP](https://metrics.torproject.org/rs.html#search/)
* [VT IP](https://www.virustotal.com/#/ip-address/)
* [X-Force IP](https://exchange.xforce.ibmcloud.com/ip/)

## Looking glasses

* [Looking.house](https://looking.house/) -- 166 Looking Glass points from 87 companies in 36 countries.

## Network diagnostic tools

* [Ping.pe](https://ping.pe) -- Ping, mtr, dig and TCP port check from multiple locations.
* [CyberChef](https://gchq.github.io/CyberChef/) -- Multiple Networking and other tools.

## Web archives

* [Archive.today](https://archive.fo/) --  Takes a 'snapshot' of a webpage that
  will always be online even if the original page disappears. Find out which
  parts of web pages are saved and other technical details here:
  https://archive.fo/faq
* [Internet Archive Wayback Machine](https://web.archive.org/) -- Captures a web
  page as it appears now for use as a trusted citation in the future. A [newer
  version](https://web.archive.org/save) supports saving error pages (HTTP
  Status=4xx, 5xx) and outlinks. Additionally their subscription service
  [Archive-It](https://www.archive-it.org/) allows institutions to build and
  preserve collections of born digital content. You can find all technical
  details here:
  https://help.archive.org/hc/en-us/categories/360000553851-The-Wayback-Machine

## Whois services

* [Cynthia.re](https://whois.cynthia.re/) -- Multi country WHOIS service.

## URLs

* [Any.Run](https://app.any.run/)
* [BlueCoat URL](http://sitereview.bluecoat.com/#/lookup-result/)
* [FortiGuard URL](http://fortiguard.com/search?q=)
* [HackerTarget](https://hackertarget.com/extract-links/)
* [TrendMicro URL](https://global.sitesafety.trendmicro.com/)
* [urlscan](https://urlscan.io/)
* [VT URL](https://www.virustotal.com/#/home/url)
* [X-Force URL](https://exchange.xforce.ibmcloud.com/url/)
* [zscaler](https://zulu.zscaler.com/)

## Hashes

* [Alien Hash](https://otx.alienvault.com/indicator/file/)
* [Hybrid Hash](https://www.hybrid-analysis.com/search?query=)
* [Talos Hash](https://talosintelligence.com/talos_file_reputation)
* [ThreatMiner Hash](https://www.threatminer.org/sample.php?q=)
* [VT Hash](https://www.virustotal.com/#/file/)
* [X-Force Hash](https://exchange.xforce.ibmcloud.com/malware/)

## Other

* [CloudFlare](http://www.crimeflare.org:82/cfs.html) -- Find IPs of CloudFlare
  hosted websites and services.
* [Digital Attack Map](https://www.digitalattackmap.com/) -- A map of daily and
  historical DDoS attacks worldwide.
* [The IP Observatory](https://ip-observatory.org) -- A platform which, can
  remotely observe, in real time, the quality of the internet at any location on
  the globe and publishes the results of these observations in clear, accessible
  visualisations.
* [Internet-Wide Scan Data Repository](https://scans.io/) -- A public archive of
  research datasets that describe the hosts and sites on the Internet.
* [Common Crawl](https://registry.opendata.aws/commoncrawl/) -- A corpus of web
  crawl data composed of over 25 billion web pages.
* [GDELT](https://registry.opendata.aws/gdelt/) -- Global Database of Events,
  Language and Tone.
* [GDELT Global Material Conflict 48-Hour Trend
  Report](https://www.gdeltproject.org/data/dailytrendreports/) -- Each morning
  the GDELT Project produces a daily global trend report on changes in Material
  Conflict across the globe, giving you an instant summary of the latest
  developments.
