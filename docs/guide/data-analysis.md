# Data analysis

This section provides documentation on the data analysis from a variety of data
sources and network measurements.

## How to use OONI data

### Tools to analyze OONI data repository

#### [ooni-sync](https://www.bamsoftware.com/software/ooni-sync/)

A Fast downloader of OONI reports using the OONI API. It works by downloading an
index of available files, and only downloading the files that are not already
present locally. You can run it again and again to keep a local directory up to
date with newly published reports.

Example usage:

The following command will create the directory "reports" if it doesn't exist,
download all `tcp_connect` reports that are not already present in the directory,
and compress the downloaded reports with xz.

`ooni-sync -xz -directory reports test_name=tcp_connect`


The query is composed of name=value pairs. Possible query parameters include:

    test_name=[name]
    probe_cc=[cc]
    probe_asn=AS[num]
    since=[yyyy-mm-dd]
    until=[yyyy-mm-dd]

The value of `test_name` can be `tcp_connect`, `web_connectivity`, `ndt`,
`dns_consistency`, `tcp_connect`, `vanilla_tor`, `meek_fronted_requests_test`,
`http_header_field_manipulation`, `http_invalid_request_line`,
`facebook_messenger`, `whatsapp`, `telegram`, `dash`, `ndt`. The query
parameters `order_by`, `order`, `offset`, and `limit` are used internally by the
program and will be overridden if present. More documentation on the API is
available from: https://api.ooni.io/api/.

Because the program only uses filename comparison to know whether a report file
has already been downloaded, it is careful not to store a file under its final
filename until it has been completely downloaded.  It is safe to interrupt the
program or run two copies simultaneously.

Tip: to generate a URL that links to a specific measurement OONI Explorer, use
this jq syntax:

`https://explorer.ooni.torproject.org/measurement/\(.report_id|@uri)?input=\(.input|join(":")|@uri)`

For example, here is how to generate a table from a directory full of reports:

```
cat reports/*.json | jq -r '[.measurement_start_time,.probe_cc,.probe_asn,.test_name,"https://explorer.ooni.torproject.org/measurement/\(.report_id|@uri)?input=\(.input|join(":")|@uri)"]|@tsv'
Or, if you used the -xz option:
xz -dc reports/*.json.xz | jq -r '[.measurement_start_time,.probe_cc,.probe_asn,.test_name,"https://explorer.ooni.torproject.org/measurement/\(.report_id|@uri)?input=\(.input|join(":")|@uri)"]|@tsv'
```

#### [jq](https://stedolan.github.io/jq/download/)

A lightweight and flexible command-line JSON processor.

#### [R](https://cran.r-project.org/)

#### ooni-sync in Docker

A Docker image with all the dependencies required to use ooni-sync, jq and R can
be found here:
https://github.com/anadahz/ooni-sync-docker

Is a software environment for statistical computing and graphics.

### Case study: Tanzania

Note: Please ensure you have enough storage capacity available as OONI reports
may occupy a fair chunk of storage from hundreds of MiB to several GiB.

#### Download OONI data

Downloading all OONI country reports by using
[ooni-sync](#ooni-sync).

Example ooni-sync command to download all OONI reports ever collected from
Tanzania (country code TZ):

```
ooni-sync -xz -directory tz-ooni-reports probe_cc=tz since=01-01-2012 limit=1000000
```

You should be seeing something similar to this at your shell output:

```
828/857 ok: tz-ooni-reports/20181218T121034Z-TZ-AS37454-ndt-20181218T121042Z_AS37454_VU9vP1oCcOuB5M04f6tR4RYUmIiJpxGQB2IkMfAy5HiralpMCD-0.2.0-probe.json.xz
829/857 ok: tz-ooni-reports/20181231T144647Z-TZ-AS327885-ndt-20181231T144657Z_AS327885_ufZGRAnBdudgh0X5Vt5EVdE5n4PEmVjFjZ6t4UycXzI4EgUT9Y-0.2.0-probe.json.xz
830/857 ok: tz-ooni-reports/20190101T125100Z-TZ-AS37133-whatsapp-20190101T125106Z_AS37133_yjhXu9xBgVaEPgB2OQS7l8oqLpNwCNjyYgwgiLtBkE7XOd66K7-0.2.0-probe.json.xz
831/857 ok: tz-ooni-reports/20190101T125120Z-TZ-AS37133-telegram-20190101T125127Z_AS37133_J8myeccAwIDF0NjY47Ixhlr03ojgyDGkF80X1l3ZrRdjQg5WdM-0.2.0-probe.json.xz
832/857 ok: tz-ooni-reports/20190101T125044Z-TZ-AS37133-http_header_field_manipulation-20190101T125049Z_AS37133_K4NutKeGRUl6wbtPvfxkkMgjAmDOYIFNr00AD2ubuGvlGNcHeK-0.2.0-probe.json.xz
833/857 ok: tz-ooni-reports/20190104T211332Z-TZ-AS327885-ndt-20190104T211341Z_AS327885_yL5dgUq4u7r7OfGTzu5qh3PQ1vMIbXSfjZAAj9Sgckajc79LiF-0.2.0-probe.json.xz
834/857 ok: tz-ooni-reports/20190104T211347Z-TZ-AS327885-ndt-20190104T211354Z_AS327885_SA5UGGReW8Nj9Dj91QPuKF478F6iLnNXt2RFDNGchJQLRjy9yn-0.2.0-probe.json.xz
```

On the same directory as the one used to download OONI reports we run the
following to extract a CSV from all downloaded data per test.

#### Analyze Web Connectivity OONI reports

A high level description of the Web Connectivity test can be found here:
https://ooni.torproject.org/nettest/web-connectivity/
The complete test specification with more details can be found here:
https://github.com/ooni/spec/blob/master/nettests/ts-017-web-connectivity.md

The following commands will generate a CSV file generated from all downloaded
OONI reports of the Web Connectivity test:

```
(echo "date,report_id,probe_cc,probe_asn,input,blocking"; xz -dc *web_connectivity*.json.xz | jq -r '[.measurement_start_time,.report_id,.probe_cc,.probe_asn,.input,.test_keys.blocking]|@csv') > web_connectivity.csv
```

You should be able to find a file of the generated CSV named:
`web_connectivity.csv`

The file should have the following CSV header:
`date,report_id,probe_cc,probe_asn,input,blocking`

* The `date` field stands for `measurement_start_time`: Timestamp of when the
  measurement was performed in UTC time coordinates (ex. 2015-08-24 12:02:23)
* `report_id`: OONI unique report identifier
* `probe_cc`: The two-letter country code of the probe as defined in ISO3166-1
  alpha-2 or ZZ when undefined (ex. "IT")
* `probe_asn`: the Autonomous System Number of the network the test is related
  to prefixed by "AS" (ex. "AS1234")
* `input`: A URL, hostname, IP
* `test_keys.blocking`: Used to identify the reason for blocking. This can be
  one of `tcp_ip`, `dns` or `http`.

Further documentation on the reason for blocking:

It will be set to `dns` if the DNS query answers are inconsistent and when doing
the HTTP request we don't get the expected page `((headers_match == false and
body_length_match == false) or status_code_match == false)`.

Moreover `dns` will be the reason for blocking when doing the HTTP request we
get a failure of type `dns_lookup_error`.

It will be set to `tcp_ip` when the DNS query answers are consistent, but we
have failed to connect to the IP:PORT combinations that have been resolved in
the experiment, while the control has succeeded. Moreover the HTTP request must
have failed.

It will be set to `http` when DNS resolutions are consistent and we are able to
establish a TCP connection to the IP ports of the control, but the HTTP request
either fails or we get back a HTTP response that contains a page we don't
expect.

The following R script snippet should be copy to a file (ex: `webct.R`):

```R
library(ggplot2)

x <- read.csv("web_connectivity.csv", stringsAsFactors=FALSE)
# Turn the "blocking" column into an average rate of blocking.
x <- aggregate(blocking ~ probe_cc + probe_asn + input, x, function(z) { sum(z != "false") / length(z) })

p <- ggplot(x, aes(x=probe_asn, y=input, fill=blocking))
p <- p + geom_tile()
p <- p + facet_grid(. ~ probe_cc, drop=FALSE, scales="free_x", space="free_x")
p <- p + scale_fill_gradient(low="#dddddd", high="midnightblue")
p <- p + theme_bw()
p <- p + theme(axis.text.x=element_text(angle=90))
ggsave("web_connectivity.pdf", p, height=30, width=10)
```

Run the script with:
`Rscript webct.R`

The script may take some time according to the number of the reports. Upon
successful completion you should be able to see the results in the plot (PDF)
file named as: `web_connectivity.pdf`

<!--
#### Examing HTTP Header Field Manipulation

XXX add test intro

This an OONI test, more info here:
https://ooni.torproject.org/nettest/http-header-field-manipulation/

Spec test info here:
https://github.com/ooni/spec/blob/master/nettests/ts-006-header-field-manipulation.md

XXX Upload scripts used to parse reports

#### Examing HTTP invalid request line

XXX add test intro

This an OONI test, more info here:
https://ooni.torproject.org/nettest/http-invalid-request-line/

Spec test info here:
https://github.com/ooni/spec/blob/master/nettests/ts-007-http-invalid-request-line.md

XXX Upload scripts used to parse reports

#### Examining TCP connection test reports

XXX add test intro

This an OONI test, more info here:
(Short of this description but not only for bridges)
https://ooni.torproject.org/nettest/tor-bridge-reachability/

Spec test info here:
https://github.com/ooni/spec/blob/master/nettests/ts-008-tcp-connect.md

XXX Upload scripts used to parse reports

#### Examing Vanilla Tor reports

XXX add test intro

This an OONI test, more info here:

https://ooni.torproject.org/nettest/vanilla-tor/

Spec test info here:
https://github.com/ooni/spec/blob/master/nettests/ts-016-vanilla-tor.md

XXX Upload scripts used to parse reports
-->
