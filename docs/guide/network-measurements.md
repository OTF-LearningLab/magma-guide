# Network measurements

Testing methodology

* [Types of network measurements](#types-of-network-measurements)
* [Public sources of data](data-sources)
* [Performing network measurements](run-measurements)

## Types of network measurements

<!-- Add Complete list of Internet censorship methodologies -->

Throughout the years a number of methodologies have been developed to detect
filtering or blocking of network resources, tampering of communication
channels and intentional manipulation of network routes.

<!--
\textit{XXX: Complete list of Internet censorship detection
methodologies}
\textit {XXX: Explain the different between active and passive Internet
censoship detection methodologies}

\textit{XXX: Active/passive list of Internet censorship
detection methodologies}
-->

### Active/passive network measurements

Though most techniques generally rely on passive detection methods that usually
visualize a small subset of potential Internet censorship events within specific
networks, countries or geographical areas. The results are usually derived from
a limited amount of vantage points that often provide an inaccurate view of the
Internet censorship landscape.

<!-- Revise this section
Research related to network measurements via side channel interferences proposes
techniques that can discover intentional packet drops ([Detecting Intentional
Packet Drops on the Internet via TCP/IP Side
Channels](https://ensa.fi/papers/Ensafi2014c.pdf), idle scans ([Antirez: new tcp
scan method], [The Official Nmap Project Guide to Network Discovery and Security
Scanning](https://ensa.fi/papers/Ensafi2014c.pdf#cite.nmapbook), [Idle Port
Scanning and Non-interference Analysis of Network Protocol Stacks Using Model
Checking](https://www.cs.unm.edu/~treport/tr/10-01/paper-2010-03.pdf), inferring
information about remote networks([Off-Path TCP Sequence Number Inference
Attack: How Firewall Middleboxes Reduce
Security](https://www.cs.ucr.edu/~zhiyunq/pub/oakland12_TCP_sequence_number_inference.pdf)
or by using the `IPID` field to measure the amount of internal traffic generated
by a server, the number of servers in a load-balanced setting, and one-way
delays ([Exploiting the IPID field to infer network path and end-system
characteristics](https://www.cs.purdue.edu/homes/ribeirob/pdf/Chen04_IPID.pdf).
Further research compounds loss on specific routes to estimate the packet loss
between arbitrary endpoints without access to those endpoints ([iPlane: An
Information Plane for Distributed
Services](https://web.eecs.umich.edu/~harshavm/iplane/).

Another approach utilizes recursive DNS queries to measure the packet loss
between a pair of DNS servers, and extrapolates from this to estimate the packet
loss rate between arbitrary hosts ([Queen: Estimating Packet Loss Rate between
Arbitrary Internet
Hosts](https://www.microsoft.com/en-us/research/publication/queen-estimating-packet-loss-rate-arbitrary-internet-hosts/)

Control plane and data plane datasets such as: BGP interdomain routing,
unsolicited dataplane traffic to unassigned address space, active macroscopic
traceroute measurements, RIR delegation files, and MaxMind’s geolocation
database have been used to determine which forms of Internet access disruption
were implemented in a given region over time ([Analysis of Country-wide Internet
Outages Caused by
Censorship](https://www.caida.org/publications/papers/2014/outages_censorship/outages_censorship.pdf).
Moreover a methodology that collects information on DNS resolution and resource
availability around the Internet by probing the IPv4 address space is used to
detect cases of ISP level DNS hijacking in a number of countries.
-->

Active Internet censorship detection methods such as network measurements
usually provide a preciser understanding of a network segment with respect to
network filtering and content blocking. Due to their nature active network
measurements are harder to be conducted as they require to be deployed in
vantage points that have access or are located within proximity of the
underlying network.

Passive internet network measurements are usually helpful when there is adequate
knowledge or evidence of an internet censorship event occurring in a specific
network segment or region where usually the data are not enough to derive safe
assumptions about the nature, methods and incentives to censor any network
resources or investigate specific network anomalies.

Whereas active network measurements from vantage points that are located outside
the investigated network can be useful and easily deployable as there are no
access barriers but the results derived from such measurements usually doesn't
represent an accurate view of the network anomalies.

### Other

* Measurements to detect internet censorship, information controls or other
  types of network filtering (eg: VPNs, anonymity networks)
* Measurements to detect network throttling, net neutrality, data
  discrimination, alternated content based on geographical location

## Hardware

This section enlists the possible types of hardware and how they can be
instrumented to perform network measurements.

### Mobile phones

Mobile phone is a hardware type with the greatest availability and ease to
perform network measurements, as the majority of the population owns or has
access to a mobile phone. One can perform network measurements from a variety of
networks given that a user can swiftly change and connect between different
networks either by mobile or with the use of SIM cards from various mobile
operators.

Orchestrating and performing longitudinal (periodic) network measurements is a
challenge in mobile phones as it requires physical attendance and manual
instrumentation of network measurements. Furthermore due to the movableness of
the device it's more prone to failures and possible errors in networks
measurements.

Mobile phone users can be easily tracked and identified by service providers,
governments and law enforcement agencies and greater attention is required when
performing Internet censorship or surveillance related network measurements
depending on their threat model.

Battery and storage capacity are as limiting factors that may affect the
successful completion and collection of data from network measurements.

### Stationary hardware devices

Stationary hardware devices include a variety of physical hardware devices from
desktop computers and servers to embedded devices. They are usually able to
perform uninterrupted and longitudinal (periodic) network measurements. They can
be instrumented so that require minimal or close to zero physical attendance to
orchestrate network measurements. Due to their fixed position it is difficult to
interchange, connect and perform network measurements to different networks.

The risks of hardware assisted network measurements are subject to the Internet
access connection type and subscription plan. Network bandwidth is often not
limited or with a sufficient network bandwidth depending on the connection type.

Hardware devices have greater storage capacity and they can commonly operate
uninterrupted to collect and store network measurements.

<!--
### Hardware type comparison
| Type | Storage | Power | Risks |
-->

## Perform network measurements

In this section we enlist the available software that can be used to perform
network measurements.

### [OONI](run-measurements#ooni)

<!-- Add Wehe subsection
### Wehe

* [DifferentiationDetector](https://github.com/FangfanLi/wehe_desktop)
-->

<!--
### Advanced measurements

* IPID
-->

## Vantage points

In order to achieve an accurate view of a specific area, access to vantage
points from network(s) within this area is required. Different types of networks
exists and can be acquired by a person, following we are going to provide a list
of all the available vantage points.

Network bandwidth limitations may apply in data communications depending on the
Internet access technology (mobile or broadband internet connection) and
subscription plan.

::: warning
You should always ask for permission to use a specific network or vantage point
for network measurements or any network related research. Furthermore please the
terms of service and acceptable usage policy of any given subscription plan or
network.
::::

### Geographically bound

This section provides a list of the vantage points that require "physical
access" to a network. These are broadband services and mobile data connections.
Both require the user or equipment to have be physically located in a given
network. Often these vantage points are operated from local or regional ISPs and
are considered of significant importance as they are the ones implementing
extensive network filtering used to block Internet websites services.
Additionally these networks are usually found to deploy different types of
surveillance equipment as they are almost merely used by citizens of the
underlying country of the vantage point location.

This is the vantage point with the greatest risk factor as usually operates
under the owner's name were the network connection is being provided.

### Remote access

Remote access type vantage points refer to the networks one can access by means
of remote access (for instance via a [secure
shell](https://en.wikipedia.org/wiki/Secure_Shell) network service or via
[remote desktop
services](https://en.wikipedia.org/wiki/Remote_Desktop_Services)). Access to
these networks can be granted by purchasing a subscription plan. Prices,
availability and plans vary from geographical location, to network bandwidth and
technical specifications of a server (storage capacity and computing power).
Listing below the most common types of networks that can be accessed remotely.

This type vantage point has a lower risk factor as the only identifying
information of the user is the payment information (if it's a paid plan) and
location of connection to the VPN. Both can be pseudo-anonymized and not provide
the exact real information.

#### VPN

A VPN (Virtual Private Network) is (a tunnelled) connection to a network that
routes all network traffic to that network. It's being for users to conceal
their real network to the Internet services or access restricted internal
services within a network. This type of vantage point offers availability to
many different network in countries and is being used due to it's simplicity to
use (typically with a user of a software) and the low cost (or no cost) for its
usage. Although VPNs provide access to different vantage point locations, the
network are not in residential thus often not subject to information controls or
other network blocking restrictions. Nevertheless the VPN market often deceit on
the real location of their networks (for instance an advertised VPN vantage
point for Afghanistan may operates from a location in Europe).

Further read:

* Razaghpanah, Abbas & Li, Anke & Filastò, Arturo & Nithyanand, Rishab &
  Ververis, Vasilis & Scott, Will & Gill, Phillipa. (2016). Exploring the Design
  Space of Longitudinal Censorship Measurement Platforms.
  [Archived PDF version](https://web.archive.org/web/20191015143811/https://arxiv.org/pdf/1606.01979.pdf)
* [VPNs are Using Fake Server Locations, Sven Taylor](https://web.archive.org/web/20190709204643/https://restoreprivacy.com/vpn-server-locations/)
* Zachary Weinberg, Shinyoung Cho, Nicolas Christin, Vyas Sekar, and Phill- ipa
  Gill. 2018. How to Catch when Proxies Lie: Verifying the Physical Loca- tions
  of Network Proxies with Active Geolocation. In 2018 Internet Measure- ment
  Conference (IMC ’18), October 31November 2, 2018, Boston, MA, USA.  ACM, New
  York, NY, USA, 15 pages. [https://doi.org/10.1145/3278532.3278551](https://doi.org/10.1145/3278532.3278551)
  [Archived PDF version](https://web.archive.org/web/20190519141734/https://www.andrew.cmu.edu/user/nicolasc/publications/Weinberg-IMC18.pdf)

#### Servers

The server and web hosting industry have been offering services in different
geographical areas as early as the existence of the Internet. Nowadays you are
able to get a server with specific technical specification in almost every part
of the world. Various types of servers exist; from [bare metal dedicated
servers](https://en.wikipedia.org/wiki/Bare-metal_server) to
[VPSes](https://en.wikipedia.org/wiki/Virtual_private_server) (Virtual Private
Servers). Furthermore one can rent a [collocation
facility](https://en.wikipedia.org/wiki/Colocation_centre) that provide space,
power, network connectivity, storage and physical security in order to host a
server or other equipment.

Servers are often located in business networks or networks that do not employ
the same network filtering and surveillance infrastructure as in the
geographically bound networks. Another limitation is the costs associated with
lease of server and rental of network bandwidth or other services especially in
locations where datacenter availability and network bandwidth are scarce.

### Vantage points comparison

| Connection Type  | Risk Factor | Cost           | Efficiency   |
|------------------|-------------|----------------|--------------|
| Broadband        | High        | Low - Medium   | High         |
| Collocation      | Low         | High           | Low - Medium |
| Dedicated Server | Low         | High           | Low - Medium |
| Mobile Broadband | High        | Medium - High  | High         |
| VPN              | Low         | Very Low - Low | Very Low     |
| VPS              | Low         | Low - Medium   | Low - Mid    |

### Sever inquiry template

This is a sample ISP inquiry template, to requests a server for conducting
network measurements:

```
Hi XXX,

I am interested in your XXX server for performing network measurements.

We are a registered association/research insitution in City XXX.
We are conducting Internet censorship research on the country/area XXX.
Our research project is: XXX
 // Explain the purpose and scope of the research here \\

Are you fine with running network measurements from your network/server?

We would be able to pay for XXX months up front or work on other
billings/subscription plan.

If you want to chat about it, you can find me on XXX.

Thank you for your time.

Kind regards,
XXX
```

### List of ISPs in various locations

* [ExoticVM](https://www.exoticvm.com/) - A list of all hosts offering VPSes in
  various geographical locations.
* [LowEndTalk](https://www.lowendtalk.com/) - A forum to request and view
  servers offers in various geographical locations and lower prices.
* [Web Hosting Talk](https://www.webhostingtalk.com/) - One of the largest web
  hosting community with offers and discussion about servers in different
  locations.

### Network measurements best practices

This section has been using the excellent [Zmap's scanning best
practices](https://github.com/zmap/zmap/wiki/Scanning-Best-Practices) to provide
suggestions for researchers conducting network measurements.

* Coordinate closely with local network administrators to reduce risks and
  handle inquiries
* Verify that network measurements will not overwhelm the local network or
  upstream provider
* Signal the benign nature of the network measurements in web pages and DNS
  entries of the source addresses Clearly explain the purpose and scope of the
  network measurements in all communications
* Provide a simple means of opting out and honor requests promptly
* Conduct network measurements no larger or more frequent than is necessary for
  research objectives
* Spread network measurements traffic over time or source addresses when feasible

It should go without saying that network measurement researchers should refrain
from exploiting vulnerabilities or accessing protected resources, and should
comply with any special legal requirements in their jurisdictions.

## Risks

The risks and ethical considerations discussed in this section are based from
research in (read in [Archived PDF
version](https://web.archive.org/web/20170811202625/https://www.oii.ox.ac.uk/archive/downloads/research/files/Ethical_Privacy_Guidelines_for_Mobile_Connectivity_Measurements.pdf):

    Zevenbergen, B., Brown, I., Wright, J., Erdos, D.O. (2013) Ethical Privacy
    Guidelines for Mobile Connectivity Measurements. Oxford Internet Institute,
    University of Oxford.

Network measurements can impact on user privacy, especially if specific data on
some aspect of user behaviour is collected. Once a dataset is disclosed online
(either accidentally or not) the researcher has lost control over how these data
will be used. The potentially sensitive data collected by network measurements
can cause harm to individuals if they are identified as the result of a dataset.

A person conducting Internet censorship or surveillance related research that
include third party persons should assess a research design and quantify the
privacy and utility trade-offs to high, medium and lower risk. The protection of
personal information must be considered at all times to ensure privacy by
design. A privacy impact assessment defines (PIA) how personal information is
handled during a research project. It addresses legal, regulatory, policy and
ethical requirements regarding privacy. An important element of the PIA is a
detailed examination and evaluation of technical safeguards and protections for
handling information to mitigate potential privacy risks such as evaluating the
effectiveness of an anonymisation technique. Moreover a PIA should address
unforeseen disclosures of datasets.

### Assessing privacy risks

#### Key definitions

A dataset is a collection of related sets of information that is composed of
separate elements. The elements of datasets discussed in this framework are
divided into three categories: (1) identifiers, (2) key attributes, and (3)
secondary attributes.

1. Identifiers are attributes that can individually distinguish the data subject
   more or less directly. Typical identifiers include: name, address, social
   security numbers, mobile phone number, IMEI number.
2. Key attributes can be used to identity a data subject using auxiliary sources
   of information, by linking to databases that contain identifying information.
   They are indirect identifiers of a data subject, which make an individual
   more distinctive in a population. Typical key attributes include: age, race,
   gender, date of birth and place of residence.
3. Secondary attributes cannot individually identify a data subject directly and
   may require significant amounts of auxiliary data to be useful for re
   identification purposes. A data subject may then be identified individually
   through more sophisticated methods such as fingerprinting, rather than mere
   linking of databases. Examples include the settings in an application, the
   battery level measured over time, or location patterns.

#### Collection of data

* Categorization of data types: Which data categories are required for the
  research, how they will be collected and processed.
* Data minimization: Only relevant and non excessive data should be ensured
  that are collected
* Risk assessment: Assess the balance of risks and benefits

#### Categorization of data types

Many types of data can be collected via network measurements. Some will identify
the user directly (identifiers). Other data types will make a re-identification
of the user likely with only a few extra pieces of auxiliary data (key
attributes), or will need to be combined with several data to re-identify the
user through methods such as fingerprinting (secondary attributes).

::: warning
Privacy risks increase when more data types are collected, as it enables
inferences to be drawn more easily. Therefore, we apply the labels described
above (identifier, key-attribute and secondary-attribute) in a general manner.
These labels should be considered as guidance rather than objectively correct
in all contexts and must therefore be discussed in detail with colleagues and/or
a legal expert.
:::

#### IMEI number

An IMEI number is the serial number of a phone, and is unique to each device. It
does not directly identify a person, but as devices are generally owned and used
solely by a specific individual, it should be considered a key-attribute.
Auxiliary data, such as billing information from the telecom operator, can
identify that device’s owner or user.

#### Current IP address

IP addresses identify devices participating in a computer network that uses the
Internet Protocol for communication. Internet service providers and telecom
operators may keep logs of IP addresses that are assigned to certain fixed line
broadband connections. IP addresses for mobile devices, however, tend to be
shared amongst multiple devices over time. It is frequently stated that mobile
carriers do not keep accurate logs of which IP-addresses were assigned to which
device at a given moment in time. This would make such data a secondary
attribute. Under certain laws and policies, however, carriers are required to
store data such as assigned IP addresses for specified periods of time,
sometimes many years, although this can be further complicated by common
techniques such as Network Address Translation (NAT) that allows multiple
devices to share a single IP address. In the case that IP address logs do
uniquely identify devices, IP addresses should be considered as key-attributes
that render a data subject identifiable.

#### Name of carrier

Relatively few mobile carriers exist in the US, with subscriber numbers ranging
into the tens or hundreds of millions per carrier. However, worldwide there are
many carriers with far fewer subscribers. This is especially true in Europe and
other parts of the world, where carriers exist that only have a few thousand
subscribers. The name of the carrier will likely only be a secondary attribute,
but the amount of auxiliary data required to identify a person will depend on
the size and type of carrier. The name of an obscure carrier could easily be the
necessary data point that allows a subscriber to be identified individually
along with other attributes.

#### Battery level

The current battery level of a mobile device is widely considered to be
irrelevant for the identifiability of a mobile phone user. However, the rate of
decay of battery power of devices, when monitored over time, allows differences
to be found. While this does require significant analysis, it is not beyond the
bounds of possibility, and may be made possible due to the perceived low
sensitivity of gathering and releasing such data.  Similarly, research has
indicated that it is possible to identify many Internet users uniquely by
analysing their browser configurations (so far only proven on desktops,). It
should always be remembered that seemingly irrelevant data types can thus become
critical in re-identification.

#### Location

A GPS location gives an accurate position of a device. Locations of mobile
phones can also be collected when the GPS is switched off, by triangulating the
position with regards to Wi-Fi access points or cell towers. When a mobile phone
moves, it is usually in the direct possession of a person, typically its owner.
It can therefore be assumed that the GPS or triangulated locations reveal the
location of a specific person at a certain time and, more importantly, the
series of locations through which the device and its owner have moved over time.
Identifiability is very dependent on the context of the geographical location
and the local population density. GPS location may not be a key-attribute, but
research has shown that human mobility traces are highly unique.  Below are some
further data types that can be collected via network measurements.

#### Other types

A non exhaustive list of possible PII:

* Bearer information
* Caller ID
* Cookies
* Current DNS resolver
* Current memory usage
* Download throughput
* How many applications are running
* IMSI (International mobile subscriber identity)
* Identify active radio antenna
* MSISDN
* Names of installed applications
* Operation system & version
* Traceroutes
* Upload throughput
* Visible networks

##### References

* Narseo Vallina-Rodriguez, Srikanth Sundaresan, Christian Kreibich, and Vern
  Paxson. 2015. Header Enrichment or ISP Enrichment?: Emerging Privacy Threats
  in Mobile Networks. In Proceedings of the 2015 ACM SIGCOMM Workshop on Hot
  Topics in Middleboxes and Network Function Virtualization (HotMiddlebox '15).
  ACM, New York, NY, USA, 25-30. DOI=http://dx.doi.org/10.1145/2785989.2786002
  [Archived PDF version](https://web.archive.org/web/20190228021005/http://www1.icsi.berkeley.edu/~narseo/papers/hotm42-vallinarodriguez.pdf)

* Mulliner, Collin. (2010). Privacy leaks in mobile phone internet access.
  10.1109/ICIN.2010.5640939.
  [Archived PDF version](https://web.archive.org/web/20180720145153/https://www.mulliner.org/collin/academic/publications/mobile_web_privacy_icin10_mulliner.pdf)

<!--
## Ethics

Ethical considerations for participants involved to contribute with network
measurements, as volunteers are (quite often) required to collect high quality
network measurements (due to geographical limitations).
-->

## Further related work

* [Networked Systems Ethics - Guidelines](http://networkedsystemsethics.net/index.php?title=Networked_Systems_Ethics_-_Guidelines)
