# Submarine and terrestrial cable visualization

## African Undersea Cables

[Many Possibilities](https://manypossibilities.net/african-undersea-cables/)
provides an up to date map of African undersea cables along with information on
the cable ownership and investors. They also provide a history map of the
African undersea cables.
## Infrapedia

[Infrapedia](https://live.infrapedia.com/) is a crowd-sourced near real-time map
of the global Internet infrastructure detailing the world's submarine and
terrestrial networks well as other critical infrastructure assets in real time
while giving ability to network professionals to connect each other to help
improve companies acquire and sell capacity faster, cheaper and easier.

## TeleGeography

[TeleGeography's interactive submarine cable
map](https://www.submarinecablemap.com/) shows the majority of active and
planned international submarine cable systems and their landing stations.
Selecting a cable route on the map provides access to data about the cable,
including the cable's name, ready-for-service (RFS) date, length, owners,
website, and landing points. Selecting a landing point provides a list of all
submarine cables landing at that station.
