# Data sources

## OONI

[OONI](https://ooni.torproject.org), the Open Observatory of Network
Interference, is a global observation network which aims is to collect high
quality data using open methodologies, using Free and Open Source Software
(FL/OSS) to share observations and data about the various types, methods, and
amounts of network tampering in the world. Since 2012, OONI has collected
millions of network measurements across more than 200 countries around the
world, a resource for researchers, journalists, lawyers, activists, advocates
and anyone interested in exploring network anomalies, such as censorship,
surveillance and traffic manipulation, and how the internet works in general.

Below a list of multiple ways to get and analyze OONI data.

### OONI explorer

[OONI Explorer](https://explorer.ooni.torproject.org/) is an easy way to get
started with OONI data. It provides a graphical data repository per country to
explore and interact with all network measurements that have been collected
through OONI probes.

* Easy to use online tool to quickly perform a fast query of OONI data
* View which websites were most recently blocked in each country
* Measurement coverage by test class and tested URL
* Search within all OONI data with different criteria: blocked URLs, anomalies,
  ASN
* Limitation: Time consuming while performing query and analyzing reports in a
  wide data range of reports

### OONI measurements API

[OONI API](https://api.ooni.io/) offers a programmatic way of accessing,
downloading and searching through OONI data.

* Access to the complete data (raw network measurements in JSON file format)
* Fastdata analysis
* Limitation: Data need to be downloaded (storage, network bandwidth required)

Further examples can found on the Data analysis section:
[How to use OONI data](data-analysis#how-to-use-ooni-data)

## RIPE

[RIPE](https://www.ripe.net/) is the regional Internet registry for Europe, the
Middle East and parts of Central Asia. As such, they allocate and register
blocks of Internet number resources to Internet service providers (ISPs) and
other organisations. They're a not-for-profit organisation that works to support
the RIPE (Réseaux IP Européens) community and the wider Internet community. The
RIPE NCC membership consists mainly of Internet service providers,
telecommunication organisations and large corporations.

They provide a variety of data sources including public measurements and BGP
announcement data.

* [BGPlay](https://stat.ripe.net/special/bgplay/) visualises BGP routing information
* Routing Information Service [(RIS) raw
  data](https://www.ripe.net/analyse/internet-measurements/routing-information-service-ris/ris-raw-data)
* [Global certificate and ROA statistics](https://certification-stats.ripe.net/)

<!--
(Resource Public Key Infrastructure (RPKI) describes an approach to build a
formally verifiable database of IP addresses and AS numbers as resources)
-->

## Wehe

[Wehe](https://dd.meddle.mobi/index.html) collects data on ISP traffic
differentation; typically bandwidth throttling. They perform network meaurements
for popular applications such as YouTube, Netflix, Amazon Prime Video, Spotify,
Skype and NBC Sports.

### Data

Wehe's data collected after November 2018 can be found here:
https://wehe-data.ccs.neu.edu/

#### Data analysis

The code and scripts used to analyze Wehe data (including a sample dataset) can
be found here: https://github.com/FangfanLi/weheAnalysisPublicRepo


## Rapid7 Labs

### FDNS dataset

[Forward DNS (FDNS) dataset](https://opendata.rapid7.com/sonar.fdns_v2/)
contains the responses to DNS requests for all forward DNS names known by
Rapid7's Project Sonar. Until early November 2017, all of these were for the
'ANY' record with a fallback A and AAAA request if neccessary. After that, the
ANY study represents only the responses to ANY requests, and dedicated studies
were created for the A, AAAA, CNAME and TXT record lookups with appropriately
named files. The file is a GZIP compressed file containing the name, type, value
and timestamp of any returned records for a given name in JSON format.

### RDNS dataset

[Reverse DNS (RDNS) dataset](https://opendata.rapid7.com/sonar.rdns_v2/)
includes the responses to the IPv4 PTR lookups for all non-blacklisted/private
IPv4 addresses.

### HTTP dataset

[HTTP GET Responses dataset](https://opendata.rapid7.com/sonar.http/) contains
the responses to HTTP/1.1 GET requests performed against a variety of IPv4
public HTTP endpoints.

### HTTPS dataset

[HTTPS GET Responses dataset](https://opendata.rapid7.com/sonar.https/) contains
the responses to HTTP/1.1 GET requests against various HTTPS ports.

### SSL datasets

#### Common port (443 port) SSL dataset

[SSL Certificates dataset](https://opendata.rapid7.com/sonar.ssl/) contains
X.509 certificate metadata observed when communicating with HTTPS endpoints.

#### Non 443 port SSL dataset

[SSL Certificates (non-443) dataset](https://opendata.rapid7.com/sonar.moressl/)
includes the X.509 certificate metadata observed when communicating with
miscellaneous non-HTTPS endpoints, such as IMAPS, POP3S or other services.

### UDP scans dataset

[UDP Scans dataset](https://opendata.rapid7.com/sonar.udp/) contains regular
snapshots of the responses to zmap probes against common UDP services.

### TCP scans dataset

[TCP Scans dataset](https://opendata.rapid7.com/sonar.tcp/) contains regular
snapshots of the responses to zmap probes against common TCP services.

## Other sources

Available data sources used to help detect a censorship event that took place or
is currently on-going.

* Center for Applied Internet Data Analysis (CAIDA): [Internet Outage Detection and Analysis (IODA)](http://www.caida.org/projects/ioda/)
* Dyn Research: [Outages Bulletin](http://b2b.renesys.com/eventsbulletin/)
* Internet-Wide Scan Data Repository: [Longterm DNS survey](https://scans.io/study/washington-dns)
* NLnet Labs [RPKI Analytics](https://www.nlnetlabs.nl/projects/rpki/rpki-analytics/)
* [Cloudflare Cirrus](https://ct.cloudflare.com/logs/cirrus) publicly auditing the TLS/SSL certificates issued by certificate authorities
* [Google Product Traffic](https://www.google.com/transparencyreport/traffic/?hl=en#expand=CG) data (via Google Transparency Reports)
* [Google Trends](https://trends.google.com) find trending searchers worldwide or per country.
* [Internet Intelligence Map](https://map.internetintel.oracle.com/)
* [NDT measurement](https://www.measurementlab.net/tools/ndt/) data (via [M-Lab](https://www.measurementlab.net/))
* [NIST RPKI deployment monitor](https://rpki-monitor.antd.nist.gov/)
* [Route Views Project](http://www.routeviews.org/) BGP announcement data archive
* [Steam stats](http://store.steampowered.com/stats/)
* [Tor Metrics](https://metrics.torproject.org/) data (which is specific to the use of [tor software](https://www.torproject.org/))
