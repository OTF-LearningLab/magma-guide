# Introduction

Welcome to magma guide.

Please select a topic (on the left sidebar) or use the search bar above to find
relevant information.

The objective of this guide is to provide a research framework to people
working on information controls, network measurements and surveillance.
The documentation of this guide is meant to be used as an activity plan, to
make informed choices regarding the required tools (including ethical and
security aspects), as well as to analyze the data produced by such tools. During
the compilation of the guide further improvements have been proposed to the
developers of existing network tools in order to increase their effectiveness in
light of measurement needs.

This guide aims to address a crucial lacuna by creating a dedicated guide for
network measurements, available offline (XXX as a PDF booklet) and online
for easy readability and web compatibility.  In particular, the guide is
conceived as an open licensed and collaborative repository that systematizes
and makes available network measurements and Internet censorship testing
methodology by drawing on the experiences and feedback from previous,
current and upcoming work in different countries and regions.

The content in this guide has been developed based upon existing examples of
best practices, wide consultation with networking researchers, activists and
technologists. The guide is constantly updated with newer content, resources,
documentation and tutorials. This website is regularly updated and synced to a
version control repository (Git) used for potential reviews, translations and
up-to-date content from reviewers of the network measurements community. Among
others, the content could be used from the community as a training material for
workshops or other non-academic events similar in nature to digital security
trainings.

Feel free to request information on a specific topic or help by
[contributing](https://gitlab.com/lavafeld/magma-guide) to the guide.

All content of the magma guide (unless otherwise mentioned) is licensed under a
[Creative Commons Attribution-ShareAlike 4.0 International
License](https://creativecommons.org/licenses/by-sa/4.0/) (CC BY-SA 4.0).

Many thanks to all that helped realize the magma guide. You can find the
complete list of contributors and reviewers [here](contributors).
