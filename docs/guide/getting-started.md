# Getting started

In this part we are going to provide information on how to starting working and
perform any Internet censorship related research for a given country or region.

## ISP lists

A list of Internet Service Provider (ISP) worldwide database such as the website
[ISP Quick List .com](https://www.ispquicklist.com/) provides information on the
connectivity types (Wireless, Broadband, DSL, ADSL, Cable, etc.) of a
geographical area or country.

Another resource that contains market share data is the Wikipedia's [list
of mobile network and satellite phone network
operators](https://en.wikipedia.org/wiki/List_of_mobile_network_operators)

## General information resources

Wikipedia articles and talk pages [Telecommunications by country
Telecommunications](https://en.wikipedia.org/wiki/Category:Telecommunications_by_country)
and [Internet censorship and surveillance by
country](https://en.wikipedia.org/wiki/Internet_censorship_and_surveillance_by_country)
usually provide some very useful information about Telecommunications and
possible censorship and surveillance per country or region. It's important to
inspect different the different languages offered per page as some languages are
more often updated or have different content than other. Most important sources
could prove quite helpful in our research.

## Submarine and terrestrial cable visualization

Please see the appropriate section with a list various resources on the topic.

Resource: [Submarine and terrestrial cable visualization](cable-visualization)

## Related research search

Often a similar or related study of the country or area of interest may have
been already researched in the past. The information may be old and outdated but
it will help us to gain some background information on previous regulations such
as gambling, LGBTQI+, copyright or cryptocurrency regulations as well as
filtering or censorship incidents. If we cannot find any information for a given
country we may look on research and reports on nearby geographically located
countries, as they usually share similar internet blocking methodologies.

## Website lists

### Archive web pages

It is a good practice to capture a specific version of a web page that can be
used as an evidence in case of content alternation or removal.

You may use Wayback Machine's Internet archive functionality to "snapshot" and
archive a web page as it appears at the time of saving. This way you can always
find the exact same version in case the web page changes or gets removed. You
can then follow the unique generated link to access the saved web page version.
You can find the section to save a page on the mid-right of:
https://web.archive.org/

Note that you should archive the original source and not any URL shortening
services (such as `goo.gl` or `bit.ly`).  In order to build high-quality
test-lists for a given country a requirement is to find a list of the most
visited websites in a country. We are going to be using publicly available data
from various sources that perform rank and visibility measuring of websites
based on the number of visitors in a given country.

Please find a list of other web archives service at the [OSINT source
section](osint-sources#web-archives).

### Quantcast's top international websites and rankings

Quantcast ranks websites based on the number of people in a country that visit
each measured website within a month. Rankings for a given country include only
web sites with traffic that Quantcast has independently verified.  Quantcast
directly measures site traffic through the implementation of the Quantcast
asynchronous tag on each web site.

Usage URL: `https://www.quantcast.com/top-sites/XX` (where `XX` is the country
code for a given country)

### Alexa's top sites

Alexa ranks websites based on a variety of criteria:

* Daily Time on Site (updated daily based on the trailing 3 months): Estimated daily time on site (mm:ss) per visitor to the site.
* Daily Pageviews per Visitor (updated daily based on the trailing 3 months): Estimated daily unique pageviews per visitor on the site
* Percent of Traffic From Search (updated daily): The percentage of all referrals that came from Search engines over the trailing month.
* Total Sites Linking In: The total number of sites that Alexa found that link to this site.

The free version of Alexa provides access to the 50 top websites per country, region and category.

Usage URL: `https://www.alexa.com/topsites/countries/XX` (where XX is the
country code for a given country)

### Curlie

Curlie is the successor of DMOZ, a multilingual open-content directory of web
links. The site and community who maintain it are also known as the Open
Directory Project. It uses a hierarchical ontology scheme for organizing site
listings. Listings on a similar topic were grouped into categories and
sub-categories. It features URLs in more than 1 million categories available in
90 languages, curated by more than 90000 editors. There are no commercial
restrictions and their work is published under a Creative Commons Attribution
3.0 Unported License.

Usage URL: `https://curlie.org/Regional/Region/Country`

::: Replace Region/Country with desired region and country

## Case study Tanzania

Our example case study is Tanzania.

### List of ISPs in Tanzania

Tanzania has a list of 11 ISP's:

| ISP name | ISP link |
| -------- | -------- |
| Africa Online | http://www.africaonline.com/countries/tz/ |
| Airtel Tanzania | http://africa.airtel.com/tanzania |
| Alink Telecom | http://www.alinktelecom.net |
| Habari Node | http://www.habari.co.tz/ |
| iWayAfrica | http://www.iwayafrica.com/ |
| Sasatel | http://www.sasatel.co.tz |
| SatCoNet | http://www.satconet.com/ |
| SimbaNET | http://www.simbanet.co.tz/ |
| Tigo | http://www.tigo.co.tz/ |
| Vodacom | http://vodacom.co.tz/ |
| Zantel | http://www.zantel.co.tz |

Source: [IspQuickList - List of Internet Service Provider in
Tanzania](https://web.archive.org/web/20190706171242/http://www.ispquicklist.com/internet-service-providers-list-in-Tanzania.aspx)

### ISP market share of Tanzanian ISPs

As of December 2018, the market share among Tanzanian mobile telephone
operators, as reported by those operators, was as follows:

| Rank | Operator | Customers | Market share percentage |
| ---- | -------- | --------- | ----------------------- |
| 1 | Vodacom Tanzania Limited (trading as "Vodacom") | 14,143,657 | 32.42 |
| 2 | MIC Tanzania Limited (trading as "tiGo") | 12,583,640 | 28.85 |
| 3 | Airtel Tanzania Limited (trading as "Airtel") | 10,954,621 | 25.11 |
| 4 | Viettel Tanzania Limited (trading as "Halotel") | 3,942,237 | 9.04 |
| 5 | Zanzibar Telecom Limited (trading as "Zantel") | 1,153,641 | 2.64 |
| 6 | Tanzania Telecommunications Company Limited (trading as "TTCL") | 711,411 | 1.63 |
| 7 | Benson Informatics Limited (trading as "Smart") | 132,292 | 0.30 |
|  | Total | 43,621,499 | 100.00 |

Source: [TCRA -
TelCom_Statistics_December_2018.pdf](https://web.archive.org/web/20190925163401/https://www.tcra.go.tz/images/documents/telecommunication/TelCom_Statistics_December_2018.pdf)

### African undersea cable map

![African undersea cable map](./assets/img/african-cables-map.jpg)

Source: [Manypossibilities.net African undersea
cables](https://web.archive.org/web/20190912235249/https://manypossibilities.net/african-undersea-cables/)

### Tanzanian website lists

#### Quantcast

Source: [Top Websites & Rankings for United Republic of Tanzania |
Quantcast](https://www.quantcast.com/top-sites/TZ)

#### Alexa

Source: [Alexa - Top Sites in
Tanzania](https://www.alexa.com/topsites/countries/TZ)

#### Curlie

Source: [Curlie - Regional: Africa:
Tanzania](https://curlie.org/Regional/Africa/Tanzania/)

### Telecommunication and postal regulations in Tanzania

* [The electronic and postal communications (online content) regulations, 2018](https://web.archive.org/web/20180504073328/https://www.tcra.go.tz/images/documents/regulations/SUPP_GN_NO_133_16_03_2018_EPOCA_ONLINE_CONTENT_REGULATIONS_2018.pdf)
* [The cybercrimes act, 2015](https://web.archive.org/web/20171119152220/https://www.tcra.go.tz/images/documents/policies/TheCyberCrimeAct2015.pdf)
* [Electronic and Postal Communications (Tele-traffic)](https://web.archive.org/web/20190925162315/https://www.tcra.go.tz/images/documents/regulations/14._The_Electronic_and_Postal_Communications_Electronic_Communications_Equipment_Standards_Regulations___2018.pdf)
* [Electronic and Postal Communications (Computer Emergency Response Team)](https://web.archive.org/web/20190925162609/https://www.tcra.go.tz/images/documents/regulations/11._GN._60___Electronic_and_Postal_Communications_Computer_Emergency_Response_Team_Regulations_2018.pdf)
* [List of online content services licenses issued by 31st July, 2018](https://web.archive.org/web/20190925164320/https://www.tcra.go.tz/images/headlines/Licensed_Online_Content_Service_Providers__31st_July_2018.pdf)
