# Testing methodology

## Blocking of websites

The most common testing methodology to detect blocking of websites is with the
use of test-lists. Test-lists are list of websites URLs used by a software that
runs network measurements and can detect if a website is blocked, how is it
being blocked and further important data such as network errors, DNS settings,
DNS entries, HTTP codes, HTTP headers do the page body may be collected to
determine the blocking method used.

One of the most commonly used test-lists is the Citizen Lab URL testing lists.
In order to propose website(s) for testing in specific counties or globally
please see: [Updating an existing test-list for a given country](#updating an
existing test-list for a given country)

#### Citizen Lab test-lists

Citizen Lab URL testing lists are URL testing lists intended to help in testing
URL censorship, divided by country codes. In addition to these local lists, the
global list consists of a wide range of internationally relevant and popular
websites, including sites with content that is perceived to be provocative or
objectionable. Most of the websites on the global list are in English. In
contrast, the local lists are designed individually for each country by regional
experts. They have content representing a wide range of categories at the local
and regional levels, and content in local languages. In countries where Internet
censorship has been reported, the local lists also include many of the sites
that are alleged to have been blocked. Further information about the testing
methodology can be found in:
[ONI Methodology, Tools, and Data FAQ](https://opennet.net/oni-faq)

A descriptive tree view list of the [Citizen Lab test-lists
repository](https://github.com/citizenlab/test-lists)

```
.
├── README.md                               Information about the test-lists repository
├── lists                                   The test-lists directory one file per country code
│   ├── <cc>.csv                            Country Code (cc) specific test-list files
│   ├── global.csv                          Global test-list specific file
│   ├── 00-LEGEND-category_codes.csv        Old category codes legend of the test-lists
│   ├── 00-LEGEND-country_codes.csv         Country codes legend of the test-lists
│   ├── 00-LEGEND-new_category_codes.csv    Current category codes legend of the test-lists
├── output                                  Directory used by scripts
└── scripts                                 Various scripts and tools
```

#### Updating an existing test-list for a given country

To learn how to contribute URLs for testing see the excellent guide:
[OONI - The test list methodology](https://ooni.torproject.org/get-involved/contribute-test-lists/)

Following a step by step process to submit URL(s) for a given country's
test-list.

1. Find the country code for the country you would like to update its test-list.

All test-lists are located in the following
[directory](https://github.com/citizenlab/test-lists/tree/master/lists).  Every
country code (CC) csv file, corresponds to a country's test-list. Please have a
look on the country codes legend
[file](https://github.com/citizenlab/test-lists/blob/master/lists/00-LEGEND-country_codes.csv)
for the available country codes.

2. Once you found the preferred test-list download it by clicking to the `Raw`
   button.

3. Open the file downloaded (filename `XX.csv`) with your favorite spreadsheet
   tool (for instance [LibreOffice
   Calc](https://www.libreoffice.org/discover/calc/) and use as separator
   options `Separated by Comma`.

4. You should now be able to see the test-list for the given country. Make new
   additions at the end of the spreadsheet file by adding entries to all
   required fields (see below). Please verify the URLs you submit are not
   already listed (use the Find option or the keyboard shortcut `Ctrl + F`).
   Every valid entry contains the following fields:

* `url`: (Required), the URL (website) entry you would like to submit to the test-list
* `category_code`: (Required), used to define the category of the URL entry
  ([see all category codes](https://github.com/citizenlab/test-lists/blob/master/lists/00-LEGEND-new_category_codes.csv))
* `category_description`: (Required), used to define the description of the category
  ([see all category descriptions](https://github.com/citizenlab/test-lists/blob/master/lists/00-LEGEND-new_category_codes.csv))
* `date_added`: (Required), the date you submitted each entry (usually today)
* `source`: (Optional), the source of the entry. It could be a name, or anonymous
* `notes`: (Optional), a comment used to describe the entry

5. Once you are ready with changes save the file with the same file name as CSV
   file format (`.csv`).

6. Open the same test-list URL as in step 2 (above) and click on the edit this
   file (pencil button). You are required to login in order to provide changes.
   You may use your credentials, create a Github user, or use the multi-user
   account `test-list-user` with password `usertosubmittestlists`.

7. Remove all visible lines and drag-drop your file to the `Edit file` section.
   You should be able to see only the contents of your file. Verify your changes
   by checking the `Preview changes` tab.

8. You should now be able to view your changes. Add a short description on the
   `Propose file change` and press the `Propose file change` button.

9. Congratulations you have just submitted a pull request with your proposed
   changes to the test-lists repository. Hold tight until your pull request gets
   reviewed.

##### Open and save CSV files

You can open (import) CSV files of test-lists with a spreadsheet application
such as LibreOffice. Detailed documentation on how to open and save CSV files in
LibreOffice can be found in:
[LibreOffice Help - Opening and Saving Text CSV Files](https://help.libreoffice.org/6.3/en-US/text/scalc/guide/csv_files.html)
